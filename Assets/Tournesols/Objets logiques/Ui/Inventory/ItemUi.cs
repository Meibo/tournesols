﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct ItemImage{
    public GameObject ImageContainer;
    public GameConditionals id;
}
public class ItemUi : MonoBehaviour
{
    public static ItemUi Instance;
    public GameObject ImagePrefab;
    private List<ItemImage> conditionalsIds;

    private void Start() {
        if (Instance != null){
            Destroy(this);
        }
        else
            Instance = this;
        conditionalsIds = new List<ItemImage>();
    }

    public void addItemImage(GameConditionals id, Sprite spriteToAdd){
        GameObject newItem = Instantiate(ImagePrefab);
        newItem.transform.SetParent(this.transform);
        Image itemImage = newItem.GetComponent<Image>();
        itemImage.sprite = spriteToAdd;
        ItemImage newObject;
        newObject.ImageContainer = newItem;
        newObject.id = id;
        conditionalsIds.Add(newObject);
    }

    public void removeItemImage(GameConditionals id){
        conditionalsIds.ForEach((ItemImage item) => {
            if (item.id == id){
                conditionalsIds.Remove(item);
                return;
            }
        });
    }
}
