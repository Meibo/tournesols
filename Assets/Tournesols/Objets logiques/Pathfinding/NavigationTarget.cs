﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationTarget : MonoBehaviour
{
    public void MovePlayerToTarget(){
        GameManager.Instance.PlayerNavigator.SetTarget(transform);
    }
}
