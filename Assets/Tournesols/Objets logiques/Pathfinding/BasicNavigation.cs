﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BasicNavigation : MonoBehaviour
{
    public Transform TargetPosition;
    private NavMeshAgent nma;
    private Animator animator;
    public float EndRotationSpeed;
    // Start is called before the first frame update
    void Start()
    {
        nma = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    public void SetTarget(Transform targetTransform)
    {
        TargetPosition = targetTransform;
        Vector3 position = targetTransform.position;
        nma.SetDestination(position);
        animator.SetBool("Walk", true);
    }

    public bool IsStopped()
    {
        return !animator.GetBool("Walk");
    }

    public float GetRemaningDistance()
    {
        return nma.remainingDistance;
    }

    private void Update()
    {
        if (TargetPosition != null)
        {
            if (nma.remainingDistance < 0.5f && animator.GetBool("Walk") == true)
            {
                animator.SetBool("Walk", false);
                // Debug.Log(TargetPosition.transform.position + " into " + TargetPosition.transform.parent.transform.position);
            }
            else if (nma.remainingDistance < 0.01f)
            {
                RotateTowards(TargetPosition.transform.parent.transform);
            }
            else if (nma.remainingDistance > 0.5f)
            {
                animator.SetBool("Walk", true);
            }
        }
    }

    private void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * EndRotationSpeed);
    }
}
