﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[System.Serializable]
public struct DialogueLine{
    public int SpeakerId;
    [TextArea]
    public string Line;
    public TMP_FontAsset FontStyle;
}

[CreateAssetMenu(fileName = "DialogueBlueprint", menuName = "Tournesols/DialogueBlueprint", order = 0)]
public class DialogueBlueprint : ScriptableObject {
    public DialogueLine[] Lines;
}
