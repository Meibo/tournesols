﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpeakerBubbleHandler : MonoBehaviour
{
    TextMeshPro text;
    public GameObject SmallBubble;
    public GameObject MediumBubble;
    public GameObject BigBubble;
    public TMP_FontAsset DefaultCharacterFont;
    public int id;
    private int i;
    private Quaternion rotation;
    private string actualLine;
    // ADD APPEND CHAR RATE TO SPEAKER MANAGER
    private void Start() {
        TextMeshPro smallText = SmallBubble.GetComponentInChildren<TextMeshPro>();
        smallText.font = SpeakManager.Instance.FontStyle;
        smallText.fontSize = SpeakManager.Instance.FontSize;
        SmallBubble.SetActive(false);

        TextMeshPro mediumText = MediumBubble.GetComponentInChildren<TextMeshPro>();
        mediumText.font = SpeakManager.Instance.FontStyle;
        mediumText.fontSize = SpeakManager.Instance.FontSize;
        MediumBubble.SetActive(false);

        TextMeshPro bigText = BigBubble.GetComponentInChildren<TextMeshPro>();
        bigText.font = SpeakManager.Instance.FontStyle;
        bigText.fontSize = SpeakManager.Instance.FontSize;
        BigBubble.SetActive(false);
        //text = GetComponent<TextMeshPro>();

        SpeakManager.Instance.addSpeaker(id, this);
        rotation = transform.rotation;
    }

    private void LateUpdate() {
        transform.rotation = rotation;
    }

    private void SetActiveBubble(int lineLength){
        SmallBubble.SetActive(false);
        MediumBubble.SetActive(false);
        BigBubble.SetActive(false);

        if (lineLength < SpeakManager.Instance.SmallBubbleMaxChar){
            SmallBubble.SetActive(true);
            text = SmallBubble.GetComponentInChildren<TextMeshPro>();
        }
        else if (lineLength < SpeakManager.Instance.MediumBubbleMaxChar)
        {
            MediumBubble.SetActive(true);
            text = MediumBubble.GetComponentInChildren<TextMeshPro>();
        }
        else{
            BigBubble.SetActive(true);
            text = BigBubble.GetComponentInChildren<TextMeshPro>();
        }
    }

    public void Speak(string line) {
        SetActiveBubble(line.Length);
        text.text = "";
        i = 0;
        actualLine = line;
        appendChar();
    }

    private void appendChar(){
        if (i < actualLine.Length){
            text.text = text.text + actualLine[i];
            i++;
            this.Invoke(appendChar, SpeakManager.Instance.SpeakRate);
        }
        else
            return;
    }

    public bool IsLineFinished(){
        if (i < actualLine.Length)
            return false;
        return true;
    }

    public void setFont(TMP_FontAsset newFont){
        TextMeshPro smallText = SmallBubble.GetComponentInChildren<TextMeshPro>();
        smallText.font = newFont;
        TextMeshPro mediumText = MediumBubble.GetComponentInChildren<TextMeshPro>();
        mediumText.font = newFont;
        TextMeshPro bigText = BigBubble.GetComponentInChildren<TextMeshPro>();
        bigText.font = newFont;
    }

    public void HideLine(){
        SmallBubble.SetActive(false);
        MediumBubble.SetActive(false);
        BigBubble.SetActive(false);
        text.text = "";
    }
}
