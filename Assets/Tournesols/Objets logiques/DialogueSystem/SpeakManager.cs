﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public struct SpeakerData
{
    public int id;
    public SpeakerBubbleHandler Speaker;
}

public class SpeakManager : MonoBehaviour
{
    public static SpeakManager Instance;
    private List<SpeakerData> speakers;
    public int FontSize;
    public TMP_FontAsset FontStyle;
    public float SpeakRate;
    private float saveSpeakRate;
    [HideInInspector]
    public bool isSpeaking = false;
    private int i;
    private SpeakerBubbleHandler sbh;
    private DialogueBlueprint actualDialogue;
    public int SmallBubbleMaxChar;
    public int MediumBubbleMaxChar;
    public event Action DialogueEnd;


    public void OnDialogueEnd(){
        DialogueEnd?.Invoke();
    }
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        else
            Instance = this;

        saveSpeakRate = SpeakRate;
        speakers = new List<SpeakerData>();
    }

    private void Update()
    {
        if (isSpeaking && Input.GetKeyDown(KeyCode.Space))
        {
            if (sbh.IsLineFinished())
            {
                SpeakRate = saveSpeakRate;
                i++;
                sbh.HideLine();
                if (i < actualDialogue.Lines.Length)
                    PlayLine(actualDialogue.Lines[i]);
                else{
                    OnDialogueEnd();
                    isSpeaking = false;
                }
            }
            else
                SpeakRate = 0f;
        }
    }

    public void addSpeaker(int id, SpeakerBubbleHandler speaker)
    {
        SpeakerData newSpeaker = new SpeakerData();
        newSpeaker.id = id;
        newSpeaker.Speaker = speaker;

        speakers.ForEach((SpeakerData sd) =>
        {
            if (sd.id == newSpeaker.id)
            {
                Debug.LogError("Error: similar speaker id detected, each speaker id should be unique");
            }
        });

        speakers.Add(newSpeaker);
    }

    public void PlayDialogue(DialogueBlueprint dialogue)
    {
        if (isSpeaking)
            return;
        i = 0;
        actualDialogue = dialogue;
        PlayLine(dialogue.Lines[i]);
        isSpeaking = true;
    }

    private void PlayLine(DialogueLine line)
    {
        sbh = speakers.Find((SpeakerData sd) =>
        {
            return sd.id == line.SpeakerId;
        }).Speaker;

        if (!sbh)
        {
            Debug.LogError("Error: Speaker Id : " + line.SpeakerId + " does not exist");
            return;
        }
        if (line.FontStyle)
            sbh.setFont(line.FontStyle);
        else if (sbh.DefaultCharacterFont)
            sbh.setFont(sbh.DefaultCharacterFont);
        sbh.Speak(line.Line);
    }

}
