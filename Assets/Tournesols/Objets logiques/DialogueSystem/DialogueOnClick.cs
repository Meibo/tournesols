﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public struct DialogueOnConditional{
    public GameConditionals[] conditionals;
    public DialogueBlueprint dialogueBlueprint;
}

public class DialogueOnClick : MonoBehaviour
{
    public DialogueOnConditional[] Doc;
    
    private void OnMouseDown() {
        
        //SpeakManager.Instance.PlayDialogue(dialogueBlueprint);
    }

    public void PlayDialogue(){
        int i = Doc.Length - 1;
        while (i >= 0){
            bool valid = true;
            for(int j = 0; j < Doc[i].conditionals.Length; j++)
                if (!GameManager.Instance.ConditionalStates[(int)Doc[i].conditionals[j]])
                    valid = false;
            if (valid){
                SpeakManager.Instance.PlayDialogue(Doc[i].dialogueBlueprint);
                return;
            }
            i--;
        }
    }
}
