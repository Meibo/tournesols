﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [HideInInspector]
    public bool[] ConditionalStates;
    public BasicNavigation PlayerNavigator;
    private string[] _conditionalsNames;
    
    public DialogueLine dialogue;
    void Start()
    {
        if (Instance != null)
            Destroy(this);
        Instance = this;

        //Debug.Log(dialogue.SpeakerId + " is saying " + dialogue.Line);

        _conditionalsNames = Enum.GetNames(typeof(GameConditionals));
        ConditionalStates = new bool[_conditionalsNames.Length];
        for (int i = 0; i < ConditionalStates.Length; i++){
            ConditionalStates[i] = false;
        }
    }

    public void ActivateConditional(int id, bool state = true){
        ConditionalStates[id] = state;
    }

    public void PrintConditionals(){
        string toPrint = "Liste des conditionels :";
        for (int i = 0; i < ConditionalStates.Length; i++){
            toPrint += " " + _conditionalsNames[i] + " is " + ConditionalStates[i].ToString() + "  | ";
        }
        Debug.Log(toPrint);
    }
}
