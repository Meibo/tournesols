﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionsAnimations : MonoBehaviour
{
    private Animator _animator;
    public AnimationClip PickUpAnimation;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void PickUp(){
        _animator.Play(PickUpAnimation.name);
    }
}
