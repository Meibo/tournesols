﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextSceneOnClick : MonoBehaviour
{
    public void GoToScene(string NextScene){
        if (NextScene != null && NextScene != "")
            SceneManager.LoadScene(NextScene);
        else
            Debug.LogError("No scene name provided");
    }
}
