﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScenePortal : MonoBehaviour
{
    public string NextScene;
    public string AfterTransitionScene;
    public Animator Fader;
    public DialogueBlueprint TransitionDialogue;
    
    public void GoToNextScene(){
        if (TransitionDialogue){
            DialogueSaveManager.savedDialogue = TransitionDialogue;
        }
        if (AfterTransitionScene != null)
            DialogueSaveManager.NextScene = AfterTransitionScene;
        if (Fader)
        {
            Fader.Play("FadeIn");
        }
        else
            LoadNextScene();
    }

    public void LoadNextScene(){
        SceneManager.LoadScene(NextScene);
    }
}
