﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour
{
    public Animator Fader;
    public NextScenePortal NextScenePortal;
    public Animator CharacterAnimator;

    private void Start() {
        TransitionBegin();
        NextScenePortal.NextScene = DialogueSaveManager.NextScene;
        SpeakManager.Instance.DialogueEnd += goToNextScene;
        SpeakManager.Instance.PlayDialogue(DialogueSaveManager.savedDialogue);
    }

    public void goToNextScene(){
        SpeakManager.Instance.DialogueEnd -= goToNextScene;
        NextScenePortal.GoToNextScene();
    }

    public void TransitionBegin(){
        CharacterAnimator.SetBool("Walk", true);
        Fader.Play("FadeOut");
    }
}
