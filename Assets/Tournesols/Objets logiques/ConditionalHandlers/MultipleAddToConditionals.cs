﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ActivateConditionals
{
    public GameConditionals[] Conditionals;
}

public class MultipleAddToConditionals : MonoBehaviour
{
    
    public ActivateConditionals[] ConditionalsToActivate;
    public Sprite[] ItemSprites = null;

    public void ActivateConditionals1(){
        AddToConditionals(0);
    }

    public void ActivateConditionals2(){
        AddToConditionals(1);
    }

    public void ActivateConditionals3(){
        AddToConditionals(2);
    }

    public void ActivateConditionals4(){
        AddToConditionals(3);
    }

    public void ActivateConditionals5(){
        AddToConditionals(4);
    }

    public void ActivateConditionals6(){
        AddToConditionals(5);
    }

    public void ActivateConditionals7(){
        AddToConditionals(6);
    }

    public void AddToConditionals(int index)
    {
        for (int i = 0; i < ConditionalsToActivate[index].Conditionals.Length; i++)
        {
            if (!GameManager.Instance.ConditionalStates[(int)ConditionalsToActivate[index].Conditionals[i]])
            {
                GameManager.Instance.ActivateConditional((int)ConditionalsToActivate[index].Conditionals[i]);
                if (ItemSprites != null && ItemSprites.Length == ConditionalsToActivate[index].Conditionals.Length)
                    if (ItemSprites[i] != null)
                        ItemUi.Instance.addItemImage(ConditionalsToActivate[index].Conditionals[i], ItemSprites[i]);
            }
        }
        GameManager.Instance.PrintConditionals();
    }
}
