﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public struct ConditionalForFunction
{
    public GameConditionals[] Conditionals;
    public ConditionalEvent Event;
}

public class ConditionalOnClickManager : MonoBehaviour
{
    public float activeDistance;
    public ConditionalForFunction[] ConditionsAndFunctionsList;
    private NavigationTarget target;

    private void Start()
    {
        target = GetComponentInChildren<NavigationTarget>();
        if (!target)
        {
            Debug.LogError("No navigation target detected, please add one");
            return;
        }
    }
    // Start is called before the first frame update
    private void OnMouseDown()
    {
        if (!target)
        {
            Debug.LogError("No navigation target detected, please add one");
            return;
        }
        if (activeDistance != 0 && Vector3.Distance(GameManager.Instance.PlayerNavigator.transform.position, target.transform.position) > activeDistance)
        {
            target.MovePlayerToTarget();
            WaitForActiveDistance();
        }
        else
            ActivateEvents();
    }

    public void GoToAndActivateEvents(){
        target.MovePlayerToTarget();
        WaitForActiveDistance();
    }

    private void WaitForActiveDistance()
    {
        this.Invoke(() =>
        {
            Debug.Log("waiting distance; is stopped = " + GameManager.Instance.PlayerNavigator.IsStopped() + " remaning distance = " + GameManager.Instance.PlayerNavigator.GetRemaningDistance());
            if (GameManager.Instance.PlayerNavigator.IsStopped() && GameManager.Instance.PlayerNavigator.GetRemaningDistance() < activeDistance)
            {
                this.Invoke(() =>
                {
                    ActivateEvents();
                }, 0.5f);
            }
            else
                WaitForActiveDistance();
        }, 0.5f);
    }

    private void ActivateEvents()
    {
        for (int i = ConditionsAndFunctionsList.Length - 1; i >= 0; i--)
        {
            bool valid = true;
            for (int j = 0; j < ConditionsAndFunctionsList[i].Conditionals.Length; j++)
            {
                if (!GameManager.Instance.ConditionalStates[(int)ConditionsAndFunctionsList[i].Conditionals[j]])
                    valid = false;
            }
            if (valid)
            {
                ConditionsAndFunctionsList[i].Event.Invoke();
                return;
            }
        }
    }
}
