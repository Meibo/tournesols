﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ConditionalEvent : UnityEvent {}

public class CheckConditionalOnClick : MonoBehaviour
{    
    public GameConditionals[] Conditionals;
    public ConditionalEvent EventToActivate;

    private void OnMouseDown() {
        bool valid = true;
        for (int i = 0; i < Conditionals.Length; i++){
            if (!GameManager.Instance.ConditionalStates[(int)Conditionals[i]]){
                valid = false;
            }
        }
        if (valid)
            ValidateConditionals();
    }

    private void ValidateConditionals(){
        EventToActivate.Invoke();
    }
}
