﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateOnClick : MonoBehaviour
{
    private Animator _animator;
    public AnimationClip OnClickAnimation;

    private void Start() {
        _animator = GetComponent<Animator>();
    }
    private void OnMouseDown() {
        //Animate();
    }

    public void Animate(){
        _animator.Play(OnClickAnimation.name);
    }
}
