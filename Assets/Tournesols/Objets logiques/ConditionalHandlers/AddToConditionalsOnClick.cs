﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddToConditionalsOnClick : MonoBehaviour
{
    public GameConditionals[] ConditionalsToActivate;
    public Sprite[] ItemSprites = null;

    private void OnMouseDown()
    {

    }

    public void AddToConditionals()
    {
        for (int i = 0; i < ConditionalsToActivate.Length; i++)
        {
            if (!GameManager.Instance.ConditionalStates[(int)ConditionalsToActivate[i]])
            {
                GameManager.Instance.ActivateConditional((int)ConditionalsToActivate[i]);
                if (ItemSprites != null && ItemSprites.Length == ConditionalsToActivate.Length)
                    if (ItemSprites[i] != null)
                        ItemUi.Instance.addItemImage(ConditionalsToActivate[i], ItemSprites[i]);
            }
        }
        GameManager.Instance.PrintConditionals();
    }
}
