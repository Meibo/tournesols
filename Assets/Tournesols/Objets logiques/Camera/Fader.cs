﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour
{
    public NextScenePortal NSP;
    public void NextScene(){
        NSP.LoadNextScene();
    }
}
