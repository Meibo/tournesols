﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPivot : MonoBehaviour
{
    [SerializeField]
    private float maxV = 0.0f;
    [SerializeField]
    private float minV = 0.0f;
    [SerializeField]
    private float maxH = 0.0f;
    [SerializeField]
    private float minH = 0.0f;
    [SerializeField]
    private float dragSpeed = 0.0f;
    private Vector3 dragOrigin;

    public Transform cameraTransform;
    public void MoveVertical(float value){
        Vector3 newRotation = transform.rotation.eulerAngles + new Vector3(value, 0, 0);
        if (newRotation.x < 180 && newRotation.x > maxV || newRotation.x > 180 && newRotation.x < 360 + minV)
            return;
        transform.rotation = Quaternion.Euler(newRotation);
    }
    public void MoveHorizontal(float value){
        Vector3 newRotation = transform.rotation.eulerAngles + new Vector3(0, value, 0);
        if (newRotation.y < 180 && newRotation.y > maxH || newRotation.y > 180 && newRotation.y < 360 + minH)
            return;
        transform.rotation = Quaternion.Euler(newRotation);
    }

    public void ZoomIn(float value){
        cameraTransform.localPosition =  new Vector3(cameraTransform.localPosition.x, cameraTransform.localPosition.y, cameraTransform.localPosition.z + value);
    }

    public void ZoomOut(float value){
        cameraTransform.localPosition =  new Vector3(cameraTransform.localPosition.x, cameraTransform.localPosition.y, cameraTransform.localPosition.z - value);
    }
    
    private void Update() {
       if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }
 
         if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            ZoomIn(0.2f);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            ZoomOut(0.2f);
        }

        if (!Input.GetMouseButton(0)) return;

        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        dragOrigin = Input.mousePosition;
        MoveHorizontal(pos.x * dragSpeed);
        MoveVertical(-pos.y * dragSpeed);
    }
}
