﻿using System;

[Serializable]
public struct CustomRangedInt {
	public int Min;
	public int Max;
}