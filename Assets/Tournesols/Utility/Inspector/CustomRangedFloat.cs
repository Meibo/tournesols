﻿using System;

[Serializable]
public struct CustomRangedFloat {
	public float Min;
	public float Max;
}