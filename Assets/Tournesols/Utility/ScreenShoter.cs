﻿using UnityEngine;

public class ScreenShoter : MonoBehaviour {
	const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";

	private string _gameHash;

	void Awake() {
		_gameHash = "";
		for (int i = 0; i < 5; i++) {
			_gameHash += glyphs[Random.Range(0, glyphs.Length)];
		}
	}

	void Update() {
		if (Input.GetKey(KeyCode.S)) {
			var time = Mathf.Round(Time.time * 1000);
			// var path = $"/ScreenShots/{time}";
			var path = $"ScreenShots/{time}.png";
			Debug.Log($"Saved screen in {path}");
			ScreenCapture.CaptureScreenshot(path);
		}
	}
}
